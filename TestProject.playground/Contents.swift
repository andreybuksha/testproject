//: Playground - noun: a place where people can play

import Foundation

// Generates a random `Int` inside of the closed interval.
extension Int {
    static func random(interval: ClosedRange<Int>) -> Int {
        let randomNumerInRange = Int(arc4random_uniform(UInt32(interval.upperBound - interval.lowerBound + 1)))
        return interval.lowerBound + randomNumerInRange
    }
}


extension Array {
    func getElementByIndex(_ index: Int) -> Array.Element? {
        if index < self.count {
            return self[index]
        }
        return nil
    }
}




// Task 2.1

// Generates a random x1, x2, ..., x15
let xArray = Array.init(repeating: 0, count: 15).map({_ in Int.random(interval: -1000...1000)})

// Формирую массив согласно формуле из задания
let yArray = xArray.map { (n) -> Double in
    let minN = xArray.min() ?? 0
    return pow(Double(n), 3)/Double(minN)
}

// Определитьсумму yi с нечётными индексами.
var summ: Double = 0
for i in stride(from: 1, through: yArray.count-1, by: 2) {
    summ += yArray[i]
}
print(summ)

// Task 2.2
let aArray = Array.init(repeating: 0, count: 15).map({_ in Int.random(interval: -1000...1000)})
let bArray = Array.init(repeating: 0, count: 5).map({_ in Int.random(interval: -1000...1000)})

// медленный метод
let cArray = aArray.flatMap({$0 < 0 ? $0 : nil}) + bArray.flatMap({$0 < 0 ? $0 : nil})
print(cArray)

// быстрый метод
var cArray2 = [Int]()
let maxElementsInArray = max(aArray.count, bArray.count)
for i in 0..<maxElementsInArray {
    if let n = aArray.getElementByIndex(i), n < 0 {
        cArray2.append(n)
    }
    if let n = bArray.getElementByIndex(i), n < 0 {
        cArray2.append(n)
    }
}
print(cArray2)
