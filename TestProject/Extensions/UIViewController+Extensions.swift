//
//  UIViewController+Extensions.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import UIKit

extension UIViewController {
    
    private static var loadingView: UIView!
    
    //MARK: - Alert
    func showAlert(title: String, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Loading View
    private static func createLoadingView() -> UIView {
        let view = UIView()
        view.frame = UIScreen.main.bounds
        
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityView.startAnimating()
        activityView.center = view.center
        view.addSubview(activityView)
        return view
    }
    
    func showLoadingView() {
        if UIViewController.loadingView == nil {
            UIViewController.loadingView = UIViewController.createLoadingView()
        }
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            if let nav = self.navigationController {
                nav.view.addSubview(UIViewController.loadingView)
            } else {
                self.view.addSubview(UIViewController.loadingView)
            }
        }
    }
    
    func hideLoadingView() {
        DispatchQueue.main.async {
            UIViewController.loadingView?.removeFromSuperview()
        }
    }
    
}
