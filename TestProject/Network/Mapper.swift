//
//  Mapper.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import Foundation

struct Mapper {
    
    let jsonDecoder: JSONDecoder!
    
    init(decoder: JSONDecoder) {
        self.jsonDecoder = decoder
    }
    
    func map<T: Decodable>(_ type: T.Type, from data: Any) -> (result: T?, error: Error?) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: [])
            let result = try jsonDecoder.decode(T.self, from: jsonData)
            return (result, nil)
        } catch {
            return (nil, error)
        }
    }
    
}
