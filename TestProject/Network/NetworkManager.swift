//
//  NetworkManager.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import Alamofire

//MARK: - URLs
private let baseUrl = "https://beta.taxistock.ru/taxik/api/"

enum CityAPIPaths: String {
    case test = "test"
}

//MARK: - Keys
enum CityAPIKeys: String {
    case cities = "cities"
}

struct NetworkManager {
    
    var mapper: Mapper!
    
    func getCities(completion:@escaping (_ result: [City]?, _ error: Error?) -> ()) {
        let url = baseUrl + CityAPIPaths.test.rawValue
        let cityKey = CityAPIKeys.cities.rawValue
        request(url: url, method: .get) { response in
            switch response {
            case .success(let value):
                guard self.mapper != nil else {completion(nil, nil); return}
                guard let responseJSON = value as? [String: Any], let citiesArr = responseJSON[cityKey] else { return }
                let mappingResult = self.mapper.map([City].self, from: citiesArr)
                if let result = mappingResult.result {
                    completion(result, nil)
                } else if let error = mappingResult.error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func request(url: String, method: HTTPMethod, parameters: Parameters? = nil, response: ((Result<Any>) -> ())?) {
        Alamofire.request(url, method: method, parameters: parameters).responseJSON { responseJSON in
            response?(responseJSON.result)
        }
    }
    
}
