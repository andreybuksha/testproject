//
//  MapController.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import UIKit
import MapKit

class MapController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: - Properties
    var city: CityDescribing!
    
    //MARK: - Constants
    var coordinateRadius: CLLocationDistance {
        return 1000
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        createAnnotation()
    }
    
    //MARK: - UI Setup
    func setupUI() {
        navigationItem.title = city.name
    }

    func createAnnotation() {
        mapView.addAnnotation(city.annotation)
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(city.annotation.coordinate, coordinateRadius, coordinateRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

}
