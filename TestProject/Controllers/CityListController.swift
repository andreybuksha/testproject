//
//  ViewController.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import UIKit

class CityListController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var citiesDataSource: CitiesDataSourceProtocol!
    var selectedCity: CityDescribing?
    
    //MARK: - Constants
    var cellHeight: CGFloat {
        return 50
    }
    var cityListToMapSegueIdentifier: String {
        return "cityListToMap"
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configureDataSource()
    }

    //MARK: - UI Setup
    func setupUI() {
        navigationItem.title = NSLocalizedString("Cities", comment: "")
    }
    
    //MARK: - DataSource
    func configureDataSource() {
        citiesDataSource = CitiesDataSource()
        citiesDataSource.delegate = self
        citiesDataSource.fetchCities()
    }
    
    @objc func updateCities() {
        citiesDataSource.fetchCities()
        
    }

    //MARK: - Routing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == cityListToMapSegueIdentifier {
            if let destinationController = segue.destination as? MapController, let city = selectedCity {
                destinationController.city = city
            }
        }
    }

}

//MARK: - CitiesDataSourceDelegate
extension CityListController: CitiesDataSourceDelegate {
    func dataSourceDidUpdate() {
        collectionView.reloadData()
    }
    
    func didStartLoadingData() {
        showLoadingView()
    }
    
    func didLoadData(_ error: Error?) {
        hideLoadingView()
        if let error = error {
            showAlert(title: NSLocalizedString("Error fetching cities", comment: ""), message: error.localizedDescription)
        }
    }
}

//MARK: - UICollectionView DataSource and Delegate
extension CityListController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return citiesDataSource.cities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CityCell.self), for: indexPath) as! CityCell
        let city = citiesDataSource.cities[indexPath.row]
        configureCityCell(cell, with: city)
        return cell
    }
    
    func configureCityCell(_ cell: CityCell, with city: CityDescribing) {
        cell.labelCityName.text = city.name
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let city = citiesDataSource.cities[indexPath.row]
        selectedCity = city
        performSegue(withIdentifier: cityListToMapSegueIdentifier, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: cellHeight)
    }
    
}

