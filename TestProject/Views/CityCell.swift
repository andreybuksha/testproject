//
//  CityCell.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import UIKit

class CityCell: UICollectionViewCell {
    
    @IBOutlet weak var labelCityName: UILabel!
    
}
