//
//  CitiesDataSource.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import Foundation

protocol CitiesDataSourceProtocol {
    var delegate: CitiesDataSourceDelegate? { get set }
    var cities: [CityDescribing] { get set }
    func fetchCities()
}

protocol CitiesDataSourceDelegate {
    func dataSourceDidUpdate()
    func didStartLoadingData()
    func didLoadData(_ error: Error?)
}

class CitiesDataSource: CitiesDataSourceProtocol {
    
    var delegate: CitiesDataSourceDelegate?
    var cities = [CityDescribing]()
    
    init() {
        setupNotifications()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(fetchCities), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc func fetchCities() {
        var networkManager = NetworkManager()
        let decoder = JSONDecoder()
        let mapper = Mapper(decoder: decoder)
        networkManager.mapper = mapper
        
        delegate?.didStartLoadingData()
        networkManager.getCities { [weak self] cities, error in
            self?.delegate?.didLoadData(error)
            if let cities = cities {
                self?.cities = cities
                self?.delegate?.dataSourceDidUpdate()
            }
        }
    }
}
