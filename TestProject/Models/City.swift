//
//  City.swift
//  TestProject
//
//  Created by Andrey Buksha on 21.12.2017.
//  Copyright © 2017 letmecode. All rights reserved.
//

import MapKit

protocol CityDescribing {
    var name: String { get }
    var annotation: MKPointAnnotation { get }
}

struct City: Codable, CityDescribing {
    var annotation: MKPointAnnotation {
        let ann = MKPointAnnotation()
        ann.title = name
        ann.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        return ann
    }
    
    var id: Int
    var name: String
    var latitude: Double
    var longitude: Double
    var spnLatitude: Double
    var spnLongitude: Double
    var lastAppAndroidVersion: Int
    var transfers: Bool?
    var clientEmailRequired: Bool?
    var registrationPromocode: Bool
    var parentCity: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "city_id"
        case name = "city_name"
        case latitude = "city_latitude"
        case longitude = "city_longitude"
        case spnLatitude = "city_spn_latitude"
        case spnLongitude = "city_spn_longitude"
        case lastAppAndroidVersion = "last_app_android_version"
        case transfers = "transfers"
        case clientEmailRequired = "client_email_required"
        case registrationPromocode = "registration_promocode"
        case parentCity = "parent_city"
    }
}
